#ifndef _SIM_PARAMS_H
#define _SIM_PARAMS_H

struct SimParams
{
	float gravity;
	float r_min;
	float eps;
	int cube_side;
	int iterations;
};

#endif