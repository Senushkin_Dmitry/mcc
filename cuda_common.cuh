#include "cuda_runtime.h"
#include <cstdio>
#include <cstdlib>

#ifndef _CUDA_COMMON_
#define _CUDA_COMMON_

extern "C"
{
	void allocateMemory(void **devPtr, size_t size)
	{
		cudaError_t error_memory;
		error_memory = cudaMalloc(devPtr, size);
		if (error_memory != cudaSuccess)
		{
			fprintf(stderr,
				"Cannot allocate GPU memory: %s\n",
				cudaGetErrorString(error_memory));
			exit(1);
		}
	}
	void freeMemory(void *devPtr)
	{
		cudaError_t error_memory;
		error_memory = cudaFree(devPtr);
		if (error_memory != cudaSuccess)
		{
			fprintf(stderr,
				"Cannot set free GPU memory: %s\n",
				cudaGetErrorString(error_memory));
			exit(2);
		}
	}
	void cudaMemCpyToHost(void *host, void *device, size_t size)
	{
		cudaError_t error_memory;
		error_memory = cudaMemcpy(host, device, size, cudaMemcpyDeviceToHost);
		if (error_memory != cudaSuccess)
		{
			fprintf(stderr,
				"Cannot copy memory to host: %s\n",
				cudaGetErrorString(error_memory));
			//exit(3);
		}
	}
	void cudaMemCpyToDevice(void *host, void *device, size_t size)
	{
		cudaError_t error_memory;
		error_memory = cudaMemcpy(device, host, size, cudaMemcpyHostToDevice);
		if (error_memory != cudaSuccess)
		{
			fprintf(stderr,
				"Cannot copy memory to device: %s\n",
				cudaGetErrorString(error_memory));
			exit(4);
		}
	}
}

#endif
