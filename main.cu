#include "MCCmodel.cuh"
#include <iostream>


using namespace std;

uint numIteration = 1000;
uint numParticles = 216000;
uint cubeSize = 60;
uint gridStep = 1;
uint gridSize = 20;
float r_min = 0.09;
float eps = 0.006;
float gravity = 0;

int main()
{


	MCCmodel * test = new MCCmodel(numParticles);

	test->setParamsLenardDjones(r_min, eps);
	test->setGravity(gravity);
	test->setCubeSide(cubeSize);
	test->setIterations(numIteration);

	test->initMemory();
	test->initStartConditions(MCCmodel::CUBE);
	test->calculate();
	//test->print(1);

	system("pause");
}