#include "MCCmodel.cuh"
#include "base_cuda.cuh"
#include <fstream>
#include <string>
#include <cuda.h>
#include <math.h>
#include <iostream>

__device__ 
bool checkNode_GPU(	int x, int y, int z, 
					int xx, int yy, int zz,	
					int index, int cnt)
{
	if (cnt != index)
	{
		switch (abs(abs(x) - abs(xx)))
		{
		case 0:
		{
				  switch (abs(abs(y) - abs(yy)))
				  {
				  case 0:
				  {
							switch (abs(abs(z) - abs(zz)))
							{
							case 0:
								return true;
							case 1:
								return true;
							default:
								return false;
							}
				  }
				  case 1:
				  {
							switch (abs(abs(z) - abs(zz)))
							{
							case 0:
								return true;
							case 1:
								return true;
							default:
								return false;
							}
				  }
				  default:
					  return false;
				  }

		}
		case 1:
		{
				  switch (abs(abs(y) - abs(yy)))
				  {
				  case 0:
				  {
							switch (abs(abs(z) - abs(zz)))
							{
							case 0:
								return true;
							case 1:
								return true;
							default:
								return false;
							}
				  }
				  case 1:
				  {
							switch (abs(abs(z) - abs(zz)))
							{
							case 0:
								return true;
							case 1:
								return true;
							default:
								return false;
							}
				  }
				  default:
					  return false;
				  }
		}
		default:
			return false;
		}
	}
	else
	{
		return false;
	}
}

__global__ 
void Acceleration_GPU(	float *x, float *y, float *z,
						float *acc_x, float *acc_y, float *acc_z,
						int nt, int N, float r_min, float eps, float gravity)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;

	if (index < N)
	{
		acc_x[index] = 0.f;
		acc_y[index] = 0.f;
		acc_z[index] = gravity;

		float xx, yy, zz, rr;
		float xi, yi, zi;

		//xi = x[index]; xi = (int)xi;
		//yi = y[index]; yi = (int)yi;
		//zi = z[index]; zi = (int)zi;

		int sh = (nt - 1) * N;
		for (int j = 0; j < N; j++)
		{
			//xx = x[j + sh]; xx = (int)xx;
			//yy = y[j + sh]; yy = (int)yy;
			//zz = x[j + sh]; zz = (int)zz;
			if (j != index)
			{
				rr = std::sqrtf((x[j + sh] - x[index + sh]) * (x[j + sh] - x[index + sh]) + (y[j + sh] - y[index + sh]) * (y[j + sh] - y[index + sh]) + (z[j + sh] - z[index + sh]) * (z[j + sh] - z[index + sh]));

				if (rr > 0.09 && rr < 0.7)
				{
					rr = (12 * eps / std::powf(rr, 2)) * (std::powf(r_min / rr, 12) - std::powf(r_min / rr, 6));
					acc_x[index] += (x[j + sh] - x[index + sh]) * rr;
					acc_y[index] += (y[j + sh] - y[index + sh]) * rr;
					acc_z[index] += (z[j + sh] - z[index + sh]) * rr;
				}
			}
		}
	}
}


__global__
void initFirstIter(float *x, float *y, float *z, float *ax, float *ay, float *az, int N, float tau)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;

	if (index < N)
	{
		x[index + N] = x[index] + 2. * ax[index] * tau * tau;
		y[index + N] = y[index] + 2. * ay[index] * tau * tau;
		z[index + N] = z[index] + 2. * az[index] * tau * tau;
	}
}

__global__ 
void Position_GPU(float *x, float *y, float *z, float *ax, float *ay, float *az, int nt, int N, float tau)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int sh_1 = (nt - 1) * N;
	//int sh_2 = (nt - 2) * N;

	x[index + nt * N] = x[index + sh_1] + ax[index] * tau * tau;
	y[index + nt * N] = y[index + sh_1] + ay[index] * tau * tau;
	z[index + nt * N] = z[index + sh_1] + az[index] * tau * tau;
}

MCCmodel::MCCmodel(){};
MCCmodel::MCCmodel(uint numParticles) : m_num_particles(numParticles){};

MCCmodel::~MCCmodel()
{
	delete[] m_host_x;
	delete[] m_host_y;
	delete[] m_host_z;

	delete[] m_host_acc_x;
	delete[] m_host_acc_y;
	delete[] m_host_acc_z;

	freeMemory(m_dev_x);
	freeMemory(m_dev_y);
	freeMemory(m_dev_z);

	freeMemory(m_dev_acc_x);
	freeMemory(m_dev_acc_y);
	freeMemory(m_dev_acc_z);
}

void MCCmodel::initMemory()
{
	m_host_x = new float[m_params.iterations * m_num_particles];
	m_host_y = new float[m_params.iterations * m_num_particles];
	m_host_z = new float[m_params.iterations * m_num_particles];

	memset(m_host_x, 0, m_params.iterations * m_num_particles * sizeof(float));
	memset(m_host_y, 0, m_params.iterations * m_num_particles * sizeof(float));
	memset(m_host_z, 0, m_params.iterations * m_num_particles * sizeof(float));

	m_host_acc_x = new float[m_num_particles];
	m_host_acc_y = new float[m_num_particles];
	m_host_acc_z = new float[m_num_particles];

	memset(m_host_acc_x, 0, m_num_particles * sizeof(float));
	memset(m_host_acc_y, 0, m_num_particles * sizeof(float));
	memset(m_host_acc_z, 0, m_num_particles * sizeof(float));


	allocateMemory((void **)&m_dev_x, m_params.iterations * m_num_particles * sizeof(float));
	allocateMemory((void **)&m_dev_y, m_params.iterations * m_num_particles * sizeof(float));
	allocateMemory((void **)&m_dev_z, m_params.iterations * m_num_particles * sizeof(float));

	allocateMemory((void **)&m_dev_acc_x, m_num_particles * sizeof(float));
	allocateMemory((void **)&m_dev_acc_y, m_num_particles * sizeof(float));
	allocateMemory((void **)&m_dev_acc_z, m_num_particles * sizeof(float));
	
	cudaMemCpyToDevice(m_host_acc_x, m_dev_acc_x, m_num_particles * sizeof(float));
	cudaMemCpyToDevice(m_host_acc_y, m_dev_acc_y, m_num_particles * sizeof(float));
	cudaMemCpyToDevice(m_host_acc_z, m_dev_acc_z, m_num_particles * sizeof(float));

}

void MCCmodel::setNumParticles(uint numParticles)
{
	m_num_particles = numParticles;
}

void MCCmodel::setGravity(float grav)
{
	m_params.gravity = grav;
}

void MCCmodel::setParamsLenardDjones(float param_1, float param_2)
{
	m_params.r_min = param_1;
	m_params.eps = param_2;
}

void MCCmodel::setCubeSide(int cubeSide)
{
	m_params.cube_side = cubeSide;
}

void MCCmodel::setIterations(int it)
{
	m_params.iterations = it;
}

void MCCmodel::initStartConditions(Shape shp)
{
	switch (shp)
	{
	case CUBE:
	{
				 createCube(m_params.cube_side);

				 print(0);

				 cudaMemCpyToDevice(m_host_x, m_dev_x, m_params.iterations * m_num_particles * sizeof(float));
				 cudaMemCpyToDevice(m_host_y, m_dev_y, m_params.iterations * m_num_particles * sizeof(float));
				 cudaMemCpyToDevice(m_host_z, m_dev_z, m_params.iterations * m_num_particles * sizeof(float));
				 //cudaDeviceSynchronize();

				 //Acceleration_GPU << < 128, 500 >> >(m_dev_x, m_dev_y, m_dev_z, m_dev_acc_x, m_dev_acc_y, m_dev_acc_z, 1, m_num_particles, m_params.r_min, m_params.eps, m_params.gravity);
				 //cudaDeviceSynchronize();
				 //initFirstIter << < 128, 500 >> > (m_dev_x, m_dev_y, m_dev_z, m_dev_acc_x, m_dev_acc_y, m_dev_acc_z, m_num_particles, 1);
				 //cudaDeviceSynchronize();
				 //print(1);
	}
	}
}

void MCCmodel::calculate()
{
	for (int i = 1; i < m_params.iterations; i++)
	{
		Acceleration_GPU <<< 1000, 216 >>>(m_dev_x, m_dev_y, m_dev_z, m_dev_acc_x, m_dev_acc_y, m_dev_acc_z, i, m_num_particles, m_params.r_min, m_params.eps, m_params.gravity);
		cudaDeviceSynchronize();
		Position_GPU <<< 1000, 216 >>>(m_dev_x, m_dev_y, m_dev_z, m_dev_acc_x, m_dev_acc_y, m_dev_acc_z, i, m_num_particles, 1);
		cudaDeviceSynchronize();

		cudaMemcpyAsync(m_host_x, m_dev_x, m_params.iterations * m_num_particles * sizeof(float), cudaMemcpyDeviceToHost);
		cudaMemcpyAsync(m_host_y, m_dev_y, m_params.iterations * m_num_particles * sizeof(float), cudaMemcpyDeviceToHost);
		cudaMemcpyAsync(m_host_z, m_dev_z, m_params.iterations * m_num_particles * sizeof(float), cudaMemcpyDeviceToHost);

		/*cudaMemCpyToHost(m_host_acc_x, m_dev_acc_x, m_num_particles * sizeof(float));
		cudaMemCpyToHost(m_host_acc_y, m_dev_acc_y, m_num_particles * sizeof(float));
		cudaMemCpyToHost(m_host_acc_z, m_dev_acc_z, m_num_particles * sizeof(float));
		*/
		cudaDeviceSynchronize();

		std::cout << i << std::endl;
		print(i);
	}
}

void MCCmodel::createCube(int cubeSide)
{
	bool check;
	int cnt = 0;
	check = true;//m_num_particles / std::pow(cubeSide, 3) == 1;

	if (check)
	{
		for (int i = 0; i < cubeSide; i++)
		{
			for (int j = 0; j < cubeSide; j++)
			{
				for (int k = 0; k < cubeSide; k++)
				{
					m_host_x[cnt] = 0.1 * i;
					m_host_y[cnt] = 0.1 * j;
					m_host_z[cnt] = 0.1 * k;

					cnt++;
				}
			}
		}
	}
}

void MCCmodel::print(int x)
{
	using namespace std;

	std::string m_path("D:/Projects_VS13/MCC/result/mcc_test.csv.");
	m_path = m_path + std::to_string(x);
	fout.open(m_path, std::ios_base::out);

	fout << "x,y,z" << endl;

	//cout << m_host_gridHash_x << endl << endl;

	for (auto i = 0; i < m_num_particles; i++)
	{
		//cout << m_host_gridHash_x[i] << endl;
		fout << m_host_x[i + x * m_num_particles] << "," << m_host_y[i + x * m_num_particles] << "," << m_host_z[i + x * m_num_particles] << endl;
	}

	fout.close();
}