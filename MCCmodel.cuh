/*
	Created by Senushkin D.S.
	CSMLAB MSU
	13.04.2016
*/

#ifndef _MCC_MODEL_CUH
#define _MCC_MODEL_CUH

#include "cuda.h"
#include "cuda_runtime.h"
#include "SimParams.h"
#include <fstream>

typedef unsigned int uint;


class MCCmodel
{

public:

	MCCmodel();
	MCCmodel(uint numParticles);
	~MCCmodel();

public:

	enum Shape{
		CUBE,
		SPHERE,
	};

public:

	/* Set params */

	void setNumParticles(uint numParticles);
	void setGravity(float grav);
	void setParamsLenardDjones(float param_1, float param_2);
	void setCubeSide(int cubeSide);
	void setIterations(int it);

public:

	/* Initialization */

	void initMemory();
	void initStartConditions(Shape shp);
	void calculate();
	void print(int x);

protected:

	/* Set shape */

	void createCube(int cubeSide);


protected:

	float *m_host_x;
	float *m_host_y;
	float *m_host_z;

	float *m_dev_x;
	float *m_dev_y;
	float *m_dev_z;

	float *m_host_acc_x;
	float *m_host_acc_y;
	float *m_host_acc_z;

	float *m_dev_acc_x;
	float *m_dev_acc_y;
	float *m_dev_acc_z;

	uint  m_num_particles;

	SimParams m_params;
	std::ofstream fout;

};

#endif